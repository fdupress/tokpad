require import NewFMap List Int IntExtra Distr Real AllCore FSet.
require (*  *) Privacy Encryption.


(* TOKEN device *)
type idkey, mask, code.
op didkey: idkey distr.
axiom didkey_lossless:
  is_lossless didkey.

op token: idkey -> mask -> code.
op verify (id: idkey, m: mask, c: code) = (token id m) = c. 
(* correctness *)
lemma ver_tok (k: idkey, m: mask):
  verify k m (token k m) by [].
(* uniq token for mask *)
lemma det_tok (k: idkey, m: mask):
  let c = token k m in
  forall c', verify k m c' => c' = c by [].

clone include Privacy with
  type mask <- mask,
  type idkey <- idkey,
  type code  <- code
  proof *.

clone include Encryption with
  type pkey <- pkey,
  type skey <- skey,
  type plain <-cred,
  type cipher<- cipher, 
  op n <- size Voters.
  (* proof *. -> see n_pos: 0 < size Voters *) 


op dmask: mask distr.
op (+)  : mask -> mask -> mask. (* addition modulo q *)
op ([-]): mask -> mask.         (* the additive inverse *)
op (-)  : mask -> mask -> mask. (* subtraction modulo q *)

op q: int. (* size of the cylic group over mask *)
op zero : mask. (* zero *)

(* AXIOMS *)

  (* Addition/Subtraction *)
  axiom mask_addC    (x y  : mask): x + y = y + x.
  axiom mask_addA    (x y z: mask): x + (y + z) = (x + y) + z.
  axiom mask_addf0   (x    : mask): x + zero = x.
  axiom mask_addfN   (x    : mask): x + -x = zero.
  axiom mask_sub_def (x y  : mask): x - y = x + -y.

axiom dmask_cyclic (x: mask):
  mu1 dmask x = (1%r/q%r)%Real.
axiom dmask_lossless:
  is_lossless dmask. 

lemma dmask_funi: is_funiform dmask.
proof.
  by move => ??; rewrite !dmask_cyclic.
qed.

(* vote count policy *)
op Rho: vote list -> result.
axiom rho_perm (L1 L2: vote list):
    perm_eq L1 L2 =>
    Rho L1 = Rho L2.


(* restriction to first id *)
op last_id['a 'b] (bb: (ident* 'a * 'b) list) : (ident * 'a * 'b) list = 
   with bb = "[]"      => []
   with bb = (::) b bb => if has (pred1 b.`1 \o get_id) bb
                         then last_id bb
                         else b :: last_id bb.
op first_id['a 'b] (bb: (ident* 'a * 'b) list) = rev (last_id (rev bb)).


(* TOKEN security *)
module BTS ={
  var mt: (mask*code) list
  var mL: mask list
  var qt: int
  var k : idkey
  var cor: bool
  var qv : int
}.

module type TSec_Oracle ={
  proc token(m: mask): code option
  proc verify(m: mask, c: code): bool option
  proc corrupt(): idkey
}.

module TSec_Oracle ={
  proc token(m: mask): code option ={
    var t <- None;
    if (BTS.qt <1){
      BTS.mt <- BTS.mt ++ [(m,token BTS.k m)];
      BTS.mL <- BTS.mL++[m];
      t <- Some (token BTS.k m);
      BTS.qt <- 1;
    }
    return t;
  }
  proc verify (m: mask, c: code): bool option ={
    var e <- None;
    if (BTS.qv <1){
     e <- Some (verify BTS.k m c);
     BTS.qv <- BTS.qv +1;
    }
    return e;
  }
  proc corrupt(): idkey ={
    BTS.cor<- true;
    return BTS.k;
  } 
}.


module type TSec_Adv (O: TSec_Oracle)={
  proc pre(): unit { }
  proc ftoken(): mask*code {O.token O.corrupt O.verify}
}.

module TSec_Exp (A: TSec_Adv)={
  module O = TSec_Oracle
  proc main(): bool ={
    var m, t;
    BTS.qv  <- 0;
    BTS.cor <- false;
    BTS.mt <- [];
    (* BTS.mL <- []; *)
    BTS.qt <- 0;
    
    A(O).pre();
    BTS.k <$ didkey;
    (m,t) <@ A(O).ftoken();
    return (verify BTS.k m t) /\ !(m,t) \in BTS.mt /\ !BTS.cor;

  }
}.

(* OneTimePad with Token voting scheme *) 
module MV (E: EncScheme): VotingScheme = {

  (* setup algorithm *)
  proc setup(): pkey * skey  = {
    var pk, sk;

    (pk, sk) <@ E.kgen();
    return (pk, sk);
  }

  (* register algorithm *)
  proc register(id: ident, pk:pkey): cred * cipher = {
    var w,c;

    w <$ dmask;
    c<@ E.enc(pk,w);
    return (w,c);
  }
  
  proc devkey(id: ident): idkey ={
    var k;
    k <$ didkey;
    return k;
  }

  (* voting algorithm *)
  proc vote(id: ident, v: vote, w: cred)
         : (ident * mask) = {
    var m;
    m <- v + w;
    return (id, m);
  }

  (* token *)
  proc token(m: mask, k: idkey): code ={
    var c;
    c<- token k m;
    return c;
  }
  (* valid  *)
  proc valid(k: idkey, b: ident*mask*code): bool ={
    return (token k b.`2) = b.`3;
  }
  (* tally algorithm *)
  proc tally(bb: (ident * mask * code) list, 
             kL: (ident, idkey) fmap, 
             cL: (ident, cipher) fmap, sk: skey): result = {

    var fbb, ubb, i, id, m,t, v, w, r;

    (* keep the first ballot for each voter VALID REGISTERED id *)
    fbb <- filter ((mem BP.regL)\o get_id) (first_id bb);
    i <- 0;
    ubb <- [];

    while (i < size fbb){
     (id,m,t) <- nth witness fbb i;
     if (verify (oget kL.[id]) m t) {
       w <@ E.dec(sk, oget cL.[id]);
       v <- m - oget w;
       ubb <- ubb++[v];
     }
     i <- i +1;
    }

    r <- Rho ubb;
    return r;
 }
}.

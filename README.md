
This repository accompanies the paper number 
"Private votes on untrusted platforms: models, attacks and provable scheme",
by Sergiu Bursuc, Constantin Cătălin Drăgan, and Steve Kremer 
to appear in the proceedings of EuroS&P 2019.

It contains the proof artefacts associated with the paper. It is very lightly
documented and unlikely to be understandable on its own. Please contact the authors
for code-specific questions. This README only contains instructions for those wanting
to verify the proof scripts using EasyCrypt.


Using a full EasyCrypt installation
--------------------------------------------------------------------

This mode of verification will be useful to those wanting to interact with the
proof script in ProofGeneral.

Please follow EasyCrypt's installation instructions (https://github.com/EasyCrypt/easycrypt/blob/1.0/README.md )
to install EasyCrypt, replacing #branch (in Step 2's opam pin instruction)
with commit hash c552f14a6609c8f7db2f32cd03b7ecae3349c4e6.

You can then run EasyCrypt on the proof scripts by running, at the root of this repository:
    make check

This will check all proofs, including those for Tokpad voting scheme and all cryptographic primites.  


Troubleshooting
--------------------------------------------------------------------

Due to differences in performance, it might be necessary to change the timeout
for SMT solvers (which is set to 3s by default). To do so, set the ECARGS
environment variable to -timeout X, where X is your chosen timeout. We have
found that a timeout of 30 suffices in most reasonable cases although it does
affect verification time significantly.